﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="databasecourse.aspx.cs" Inherits="Assignment2a.databasecourse" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincss" runat="server">
        <h2>Database</h2>
     <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        <div class="carousel-item item active">
          <img class="d-block w-100" src="resources/datarelationship.jpg" alt="First slide">
        </div>
        <div class="carousel-item item">
          <img class="d-block w-100" src="resources/leftjoin.jpg" alt="Second slide">
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    
    
    
    
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="concept" runat="server">
     <h3>Concept</h3>
     <p>The Left join returns all rows from the left table and the matching records from right table, 
        if join condition is met.</p>


</asp:Content>
 

<asp:Content ID="Content3" ContentPlaceHolderID="mycode" runat="server">
 
    <asp:webbox runat="server" CodeToServe="my_database"></asp:webbox>    
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="links" runat="server">
     <h3>Useful Links</h3>
    <div>
    <a href="#" class="mylinks">https://docs.oracle.com/javadb/10.6.2.1/ref/rrefsqlj18922.html</a>
    </div>
    <a href="#" class="mylinks">http://www.oracletutorial.com/oracle-basics/oracle-left-join/</a>



</asp:Content>
