﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment2a.usercontrols
{
    public partial class webBox : System.Web.UI.UserControl
    {

        public string CodeToServe
        {
            get { return (string)ViewState["CodeToServe"]; }
            set { ViewState["CodeToServe"] = value; }
        }

        DataView CreateCodeSource()
        {

            //debug.InnerHtml = CodeToServe;
            DataTable codedata = new DataTable();


            //First column is the line number of the code (idx -> index)
            DataColumn idx_col = new DataColumn();

            //SEcond column is the actual code itself
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            idx_col.ColumnName = "Line";
            idx_col.DataType = System.Type.GetType("System.Int32");

            codedata.Columns.Add(idx_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            codedata.Columns.Add(code_col);

            List<string> code = new List<string>();

            //These Tildas will be replaced with line indents
            //When the code lines are bound to rows.
            //If you have double quotes in your html code, escap them with \
            //eg. <div style=\"background:green;\"></div>
            List<string> database_code = new List<string>(new string[]{

                    "This query returns how many owners  each car has.",
                    "select  make,model,vin, count(clients.clientid)",
                    " from cars",
                    "~left join carsxclients ",
                    "~~on cars.carid=carsxclients.carid ",
                    "left join clients" +
                    " on clients.clientid=carsxclients.clientid",
                    "group by make, model, vin;"});


            List<string> webdesign_code = new List<string>(new string[]{

            "My code",
            " #authortext{ ",
            "clear: right;",
            "}",
            "#new-block img{",
            "float:left;",
            " }",

            });

            List<string> webprog_code = new List<string>(new string[]{



             " This code runs until shipping total amount reaches 35.",
             " var cartItems=[];",
             " var shoppingTotal = 0;",
             " var shoppingCost = 0;",
            "while (shoppingTotal <= 35)   {",
          // " shoppingCost = prompt/("please enter purchase amount");/",
           " var a = parseInt(shoppingCost);",
           " cartItems.push(a);",
           " shoppingTotal = shoppingTotal + a;",
           " console.log(a);",
                });



            if (CodeToServe == "my_js")
            {
                code = webprog_code;
            }
            else if (CodeToServe == "my_webdesign")
                
            {
                code = webdesign_code;
            }
            else if (CodeToServe == "my_database")
            {
                code = database_code;
            }

            

            int i = 0;
            foreach (string code_line in code)
            {
                coderow = codedata.NewRow();
                coderow[idx_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                codedata.Rows.Add(coderow);
            }


            DataView codeview = new DataView(codedata);
            return codeview;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DataView ds = CreateCodeSource();
            code_container.DataSource = ds;

            /*Some formatting in the codebehind*/


            code_container.DataBind();



        }
    }
}
