﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Webdesign.aspx.cs" Inherits="Assignment2a.Webdesign" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincss" runat="server">
        <h2>Web Design</h2>
    
    
    <div class="carousel-item item active">
           <img class="d-block w-100" src="Resources/CSSlogo.jpg" alt="First slide">
     </div>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="concept" runat="server">
    
   
    
    
    <h3>Concept</h3>
    
     <p> I find positioning elements with 'float' a little bit treaky.  It looks easy, 
         but some times it has unintended consequences such as moving section b when you try to move section a. 
         In my assignment I put float for my main section, because I wanted it to be on the right side, 
        however the footer section moved to the left top side of the page.  I discovered later you have to use clear.</P>
    

</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="teachercode" runat="server">
    
   <asp:webbox runat="server" CodeToServe="my_webdesign"></asp:webbox>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="links" runat="server">
     
        <h3>Useful Links</h3>
    <div>
    <a href="#" class="mylinks">https://www.w3schools.com/js/js_loop_while.asp</a>
    </div>
    <a href="#" class="mylinks">https://stackoverflow.com/questions/16228470/javascript-while-loop-with-if-statements</a>

</asp:Content>