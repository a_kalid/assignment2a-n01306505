﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Webprog.aspx.cs" Inherits="Assignment2a.Webprog" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincss" runat="server">
       <h2>Web Programming</h2>
  <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        <div class="carousel-item item active">
           <img class="d-block w-100" src="Resources/JSlogo.jpg" alt="First slide">
        </div>
        <div class="carousel-item item">
           <img class="d-block w-100" src="Resources/while-loop.png" alt="Second slide">
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
           
    
      

         <p>Java Script<p/>
         <p> WHILE LOOP</p>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="concept" runat="server">
     <h3> Concept</h3>
    <h5> While loop example code</h5>
    <p>While loop runs until a condition is met</p>
    
   
           
                
       
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mycode" runat="server">
     <asp:webbox runat="server" CodeToServe="my_js"></asp:webbox>
             
 
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="teachercode" runat="server">
 
 <!--          <h3> Teacher's while loop example</h3>
           <p>var whileTest=false;</p>
           <p>while (whileTest === false){</p>
           <p>var confirmCheck = confirm("Okay");</p>
           <p>if (confirmCheck === true){</p>
	       <p>whileTest = True;}}</p>
           <p>As long as this loop finds whileTest to be false, it will keep trying to confirm.</p>-->

    
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="links" runat="server">
   
    <h3>Useful links</h3>
        <div>
        <a href="#" class="mylinks">https://www.w3schools.com/js/js_loop_while.asp</a>
        </div>
        <a href="#" class="mylinks">https://www.tutorialspoint.com/javascript/javascript_while_loop.htm</a>
    
</asp:Content>

